## hello demo
---

The example presents a hello example from official, please refer to [docs](https://milvus.io/docs/example_code.md).

### Run hello example

Launch the milvus services.
```bash
docker-compose -f deploy/docker/docker-compose-cpu.yml up -V --remove-orphans
```

Install requirements via pip.
```bash
pip install -r requirements.txt
```

Finally, execute hello example.
```bash
python3 hello_milvus.py
```
