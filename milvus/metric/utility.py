import argparse
import time
from dataclasses import dataclass, field
from multiprocessing import Pool, current_process
from typing import Any, Callable, Dict

import numpy as np
import psutil
from loguru import logger
from pymilvus import (
    Collection,
    CollectionSchema,
    DataType,
    FieldSchema,
    connections,
    settings,
    utility,
)


@dataclass
class MetricConfig:
    process_num: int = 1
    """
    The number of processes to use for the metric.
    """

    num_entities: int = 3000
    """
    The number of entities to use for the metric.
    """

    query_num: int = 3000
    """
    The number of queries to use for the metric.
    """

    dim: int = 8
    """
    The dimension of the vectors.
    """

    collection_name: str = "dummy_alias"
    """
    The name of the metric collection.
    """

    index_params: Dict[str, Any] = field(default_factory=dict)
    """
    The parameters for the index. The Index params must match the search params.
    """

    search_params: Dict[str, Any] = field(default_factory=dict)
    """
    The parameters for the search. The search params must match the index params.
    """

    search_topk: int = 20
    """
    The number of results to return for the every search.
    """

    metrics: Dict[str, Any] = field(default_factory=dict)
    """
    The metrics to store.
    """


def create_vectors(num_entities, dim, seed) -> np.ndarray:
    """
    Generate random vectors of a specified size.

    Parameters:
        num_entities (int): The number of entities to create vectors for.
        dim (int): The dimension of the vectors.

    Returns:
        numpy.ndarray: An array of random vectors with shape (num_entities, dim).
    """
    rng = np.random.default_rng(seed=int(seed))
    return rng.random((num_entities, dim))


def available_resources() -> dict:
    """
    Returns a dictionary of available system resources.

    Returns:
        dict: A dictionary containing information about CPU cores, CPU frequency, and available memory.
    """
    # Get the CPU frequency
    cpu_freq = psutil.cpu_freq()

    # Construct and return the dictionary of available resources
    return {
        "cpu_cores": f"{psutil.cpu_count(logical=False)} logical cores",
        "max cpu_freq": f"{cpu_freq.max/1000:.2f} GHz",
        "min cpu_freq": f"{cpu_freq.min/1000:.2f} GHz",
        "current cpu_freq": f"{psutil.cpu_freq().current/1000:.2f} GHz",
        "available_mem_gb": f"{psutil.virtual_memory().available / (1024 ** 3):.2f} GB",
    }


def prepare_search(
    metric_config: MetricConfig,  # Argument: MetricConfig object for metric configuration
    embeddings: np.ndarray,  # Argument: Array of embeddings
) -> None:  # Return type: None
    """
    Create a collection with fields 'id' and 'embeddings' for search preparation.

    Args:
        metric_config (MetricConfig): The configuration for the metric.
        embeddings (np.ndarray): The array of embeddings.

    Returns:
        None
    """
    logger.info("Creating collection with fields 'id' and 'embeddings'")
    schema = CollectionSchema(
        [
            FieldSchema(
                name="id",
                dtype=DataType.INT64,
                is_primary=True,
                auto_id=True,
            ),
            FieldSchema(
                name="embeddings",
                dtype=DataType.FLOAT_VECTOR,
                dim=metric_config.dim,
            ),
        ],
        f"{metric_config.collection_name} collection is used by the simplest search test based on the vector database milvus",
    )
    logger.info("Connecting to Milvus...")
    connections.connect(port=settings.Config.DEFAULT_PORT)

    if utility.has_collection(metric_config.collection_name):
        logger.info(f"Collection {metric_config.collection_name} already exists.And it will be replaced.")
        utility.drop_collection(metric_config.collection_name)

    metric_collection = Collection(
        metric_config.collection_name,
        schema,
        settings.Config.MILVUS_CONN_ALIAS,
    )

    logger.info("Inserting data...")
    insert_time = time.time()
    metric_collection.insert([embeddings])
    metric_collection.flush()
    logger.info(
        f"Insertion and flush elapsed time: {time.time() - insert_time:.4f}s, "
        f"Number of entities: {metric_collection.num_entities}"
    )

    logger.info("Creating index...")
    metric_collection.create_index(
        field_name="embeddings",
        index_params=metric_config.index_params,
    )
    metric_collection.load()
    metric_collection.flush()


def init_pool(config: MetricConfig) -> None:
    """
    Initializes a connection pool and a metric collection for the given configuration.

    Args:
        config (MetricConfig): The configuration object containing the necessary parameters for initializing the pool.

    Returns:
        None
    """
    alias = current_process().name
    connections.connect(port=settings.Config.DEFAULT_PORT, alias=alias)
    global metric_collection
    metric_collection = Collection(name=config.collection_name, using=alias)


def do_search(config: MetricConfig, metric: Callable, query_vector: list, idx: int) -> float:
    """
    Perform a search in the Milvus collection using a query vector and an index.

    Args:
        config (MetricConfig): The configuration object containing search parameters.
        metric (Callable): The metric callable to use for the search.
        query_vector (list): The query vector to search for.
        idx (int): The index to use for the search.

    Returns:
        float: The time taken for the search operation in milliseconds.
    """
    import time

    # Start the timer
    start_time = time.time()

    # Perform the search using the provided metric callable
    metric(config, metric_collection, query_vector, idx)

    # Stop the timer and calculate the time taken
    end_time = time.time()
    return (end_time - start_time) * 1000


def do_metric_on_config(config: MetricConfig, embeddings: np.ndarray, metric: Callable) -> None:
    """
    Perform metric analysis on the given configuration and embeddings.

    Args:
        config (MetricConfig): The configuration for the metric analysis.
        embeddings (np.ndarray): The embeddings to be analyzed.
        metric (Callable): The metric callable to be applied.

    Returns:
        None

    Raises:
        None
    """
    # Log start of metric analysis
    logger.info("Start search metric analysis...")

    # Available resources
    resources = available_resources()

    # List to store search results
    search_results = []

    # Start time of metric analysis
    metric_start_time = time.time()

    # Create a process pool with specified number of processes and initializer function
    pool = Pool(
        processes=config.process_num,
        initializer=init_pool,
        initargs=(config,),
    )

    # Iterate over each item in embeddings
    for idx, item in enumerate(embeddings):
        # Apply the search function asynchronously and store the result
        search_results.append(
            pool.apply_async(
                func=do_search,
                args=(config, metric, item, idx),
            )
        )

    # Close the pool to prevent any more tasks from being submitted
    pool.close()

    # Wait for all the tasks to complete
    pool.join()

    # List to store the search elapsed times
    all_time = []

    # Get the result from each search task and store the elapsed time
    for result in search_results:
        search_elapsed_time: float = result.get()
        all_time.append(search_elapsed_time)

    # Import pandas library
    import pandas as pd

    # Create a dataframe with the search times
    df_search_time = pd.DataFrame({"search_time": all_time})

    # Update the metrics dictionary in the config object
    config.metrics[f"{metric.__name__}"] = {
        "resources": resources,
        "avg": f"{(time.time() - metric_start_time) * 1000 / config.query_num:.2f}ms",
        "mean": f"{df_search_time.mean()[0]:.2f}ms",
        "p99": f"{df_search_time.quantile(0.99)[0]:.2f}ms",
        "p999": f"{df_search_time.quantile(0.999)[0]:.2f}ms",
    }


def metric_with_config(metric_config: MetricConfig, metric: Callable) -> None:
    """
    Generate a metric using the provided metric configuration.

    Args:
        metric_config (MetricConfig): The configuration for the metric.
        metric (Callable): The metric callable to be used.

    Returns:
        None
    """
    # Log the metric configuration
    logger.info(f"Config: {metric_config}")

    # Prepare embeddings
    logger.info("Prepare embeddings...")
    embeddings = create_vectors(metric_config.num_entities, metric_config.dim, settings.Config.DEFAULT_PORT)
    logger.info(f"Shape of embeddings: {embeddings.shape}")

    # Prepare for metric search
    prepare_search(metric_config, embeddings)

    # Perform metric calculation
    do_metric_on_config(metric_config, embeddings, metric)


def parse_arguments() -> Dict[str, Any]:
    """
    Parses command-line arguments and returns them as a dictionary.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--process_num", type=int, default=1, help="Number of processes")
    parser.add_argument("--vector_num", type=int, default=10000, help="Number of vectors")
    parser.add_argument("--query_num", type=int, default=10000, help="Number of queries")
    parser.add_argument("--dim", type=int, default=128, help="Dimensionality")
    return parser.parse_args()
