# This presents a metric on search, query based on the vector database milvus.

from loguru import logger
from pymilvus import Collection
from utility import MetricConfig, metric_with_config, parse_arguments


def search_metric(
    config: MetricConfig,  # type hint for the config parameter
    collection: Collection,  # type hint for the collection parameter
    query_vector: list,  # type hint for the query_vector parameter
    idx: int,  # type hint for the idx parameter
) -> None:  # type hint for the return type
    """
    Perform a search in the Milvus collection using a query vector and an index.

    Args:
        config (MetricConfig): The configuration object containing search parameters.
        collection (Collection): The Milvus collection to search in.
        query_vector (list): The query vector to search for.
        idx (int): The index to use for the search.
    """

    # Perform the search operation
    _ = collection.search(
        data=[query_vector],
        anns_field="embeddings",
        param=config.search_params,
        limit=config.search_topk,
    )


def simulate_streaming_metric(
    config: MetricConfig,  # Argument: Configuration object containing search parameters
    collection: Collection,  # Argument: Milvus collection
    query_vector: list,  # Argument: Query vector for search
    idx: int,  # Argument: Index to use for search
) -> None:  # Return type: None
    """
    Perform a search and an insertion in the Milvus collection using a query vector and an index.

    Args:
        config (MetricConfig): The configuration object containing search parameters.
        collection (Collection): The Milvus collection to perform the operations on.
        query_vector (list): The query vector to search for.
        idx (int): The index to use for the search.
    """

    # Perform the search operation
    _ = collection.search(
        data=[query_vector],
        anns_field="embeddings",
        param=config.search_params,
        limit=config.search_topk,
    )

    # Perform the insert operation
    collection.insert([[query_vector]])


if __name__ == "__main__":
    logger.info("Start benchmark")
    args = parse_arguments()

    logger.info("Prepare metric config...")
    metric_config = MetricConfig(
        process_num=args.process_num,
        num_entities=args.vector_num,
        query_num=args.query_num,
        dim=args.dim,
        collection_name="search_test",
        index_params={
            "metric_type": "IP",
            "index_type": "HNSW",
            "params": {"efConstruction": 128, "M": 16},
        },
        search_params={
            "metric_type": "IP",
            "params": {"ef": 128},
        },
    )

    # Do metric with config on the scenario search_metric and simulate_streaming
    metric_with_config(metric_config, search_metric)
    metric_with_config(metric_config, simulate_streaming_metric)

    logger.info(f"metrics from config: {metric_config.metrics}")
