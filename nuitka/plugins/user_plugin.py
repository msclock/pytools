# A user plugin to print a line when a math function is called.
#
# Usage::
#
#   python -m nuitka --user-plugin=plugins/user_plugin.py --trace-my-plugin script.py
#

from nuitka.plugins.PluginBase import NuitkaPluginBase


class NuitkaPluginMine(NuitkaPluginBase):
    # Derive from filename, but can and should also be explicit.
    plugin_name = __name__.split(".")[-1]

    def __init__(self, trace_my_plugin):
        # demo only: extract and display my options list
        # check whether some specific option is set

        self._check = trace_my_plugin
        self.info(" 'trace' is set to '%s'" % self._check)

        # do more init work here ...

    @classmethod
    def addPluginCommandLineOptions(cls, group):
        group.add_option(
            "--trace-my-plugin",
            action="store_true",
            dest="trace_my_plugin",
            default=False,
            help="This is show in help output.",
        )

    def onModuleSourceCode(self, module_name, source_code):
        # if this is the main script and tracing should be done ...
        if module_name == "__main__" and self._check:
            self.info("")
            self.info(" Calls to 'math' module:")
            for num, line in enumerate(source_code.splitlines()):
                if "math." in line:
                    self.info(" %i: %s" % (num + 1, line))
            self.info("")
        return source_code
