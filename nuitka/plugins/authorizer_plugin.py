# A user plugin to add authorization validation.
#
# Usage::
#
#   python -m nuitka --user-plugin=plugins/authorizer_plugin.py --add-validation script.py
#

from nuitka.plugins.PluginBase import NuitkaPluginBase

authorizer_codes = """\
import sys
import json
import base64
from json.decoder import JSONDecodeError

from loguru import logger
from pyauthorizer.encryptor.utils import get_id_on_mac
from pyauthorizer.encryptor import interface
from pyauthorizer.encryptor.base import Token, TokenStatus

config_dict = {
    "id": get_id_on_mac(),
}

encryptor = interface.get_encryptor('simple')
dump_encodes = json.dumps(config_dict).encode("utf-8")
authorizing_token = base64.urlsafe_b64encode(dump_encodes).decode("utf-8")
authorizing_info = {
    "token": authorizing_token,
}
logger.info(f"Authorizing info: {authorizing_info}")

token_data = {}
try:
    with open("license.json") as f:
        token_data = json.load(f)
    token = Token(**token_data)
except (TypeError, FileNotFoundError, JSONDecodeError) as err:
    if isinstance(err, FileNotFoundError):
        err_msg = f"open license.json: {err.args}"
    elif isinstance(err, JSONDecodeError):
        err_msg = f"invalid license.json: {err.args}"
    else:
        err_msg = err.args
    logger.error(f"Init Token failed with {err_msg}")
    sys.exit(2)

status = encryptor.validate_token(token, authorizing_info)
if status == TokenStatus.ACTIVE:
    logger.info("Token is active")
elif status == TokenStatus.EXPIRED:
    logger.warning("Token has expired")
    sys.exit(1)
else:
    logger.error("Token is invalid")
    sys.exit(2)
"""


class AddLicenseNuitkaPlugin(NuitkaPluginBase):
    plugin_name = __name__.split(".")[-1]

    def __init__(self, add_validation):
        self._add_validation = add_validation
        self.info(" 'trace' is set to '%s'" % self._add_validation)

    @classmethod
    def addPluginCommandLineOptions(cls, group):
        group.add_option(
            "--add-validation",
            action="store_true",
            dest="add_validation",
            default=False,
            help="If add license validation to main script.",
        )

    def onModuleSourceCode(self, module_name, source_code):
        if module_name == "__main__" and self._add_validation:
            self.info("")
            self.info(" Add license validation...")
            source_code = authorizer_codes + "\n" + source_code
            self.info(f"\n{source_code}")
            self.info("")
        return source_code

    def getImplicitImports(self, module):
        yield "pyauthorizer.encryptor.builtin.simple"
        yield "pyauthorizer.encryptor.builtin.multiple"
