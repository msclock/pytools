import os


def pwd():
    """
    Returns the current path.
    """
    return os.getcwd()
