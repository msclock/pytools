import math

from loguru import logger
from tools.utils import pwd


def talk(message: str):
    """
    Generate a talk message.

    Args:
        message (str): The message to be included in the talk.

    Returns:
        str: The generated talk message.
    """
    return "Talk " + message


def main():
    message = talk(f"Hello World! The result of 2*2 is {math.pow(2,2)}")
    logger.info(f"{message} from path {pwd()}")


if __name__ == "__main__":
    main()
