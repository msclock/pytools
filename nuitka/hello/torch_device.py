import torch
from loguru import logger


def torch_device():
    """
    Set the device to be used for torch operations based on the availability of CUDA.

    Returns:
        torch.device: The device to be used for torch operations.
    """
    if torch.cuda.is_available():
        device = torch.device("cuda")
        logger.info(f"Using GPU:{torch.cuda.get_device_name()}")
        logger.debug(f"Using device, meta from device: index is {device.index}, type is {device.type}")
    else:
        device = torch.device("cpu")
        logger.info("Using CPU")

    return device


if __name__ == "__main__":
    torch_device()
