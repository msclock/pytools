site_name: PyTools Collections
site_url: https://gitlab.com/msclock/pytools
site_author: msclock

repo_name: pytools
repo_url: https://gitlab.com/msclock/pytools
edit_uri: edit/master/

docs_dir: .

# Copyright
copyright: Copyright &copy; 2023 msclock

theme:
  name: material

  features:
    - navigation.top
    - navigation.path
    - content.action.edit
    - content.action.view
    - content.code.copy
    - content.code.select
    - navigation.footer
    - navigation.instant
    - toc.follow
    - content.tooltips

  palette:
    # Palette toggle for automatic mode
    - media: "(prefers-color-scheme)"
      toggle:
        icon: material/brightness-auto
        name: Switch to light mode

    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode

  languages: en

  icon:
    logo: material/book-open-page-variant
    repo: fontawesome/brands/gitlab

extra:
  generator: false

plugins:
  - search
  - same-dir
  - git-revision-date-localized:
      enable_creation_date: true
      type: date
  - mkdocs-jupyter:
      execute: false
      include: ["*.ipynb"] # Default: ["*.py", "*.ipynb"]
      theme: dark

markdown_extensions:
  - admonition
  - abbr
  - attr_list
  - md_in_html
  - pymdownx.details
  - pymdownx.superfences
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.inlinehilite
  - pymdownx.snippets:
      check_paths: true
      auto_append:
        - docs/includes/abbreviations.md

exclude_docs: |
  docs/includes
  nuitka/README.md
  nuitka/hello/tools/README.md

nav:
  - Overview: README.md
  - docs/nuitka.md
  - docs/milvus.md
  - Reference:
    - nuitka/hello/README.ipynb
    - milvus/metric/README.ipynb
