## [1.0.32](https://gitlab.com/msclock/pytools/compare/v1.0.31...v1.0.32) (2024-01-27)


### Dependencies

* **deps:** update dependency msclock/gitlab-ci-templates to v2.6.6 ([ab7e370](https://gitlab.com/msclock/pytools/commit/ab7e37075302895b772ca0824752ec6b78bfea06))
* **deps:** update dependency pymilvus to v2.3.6 ([6be2f82](https://gitlab.com/msclock/pytools/commit/6be2f824ab652861a680bc67665e9ee9c49301bf))
* **deps:** update milvusdb/milvus docker tag to v2.3.7 ([429a33b](https://gitlab.com/msclock/pytools/commit/429a33bb7fddbf06ab222af3bec52c5d01a9d251))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.14 ([345dd56](https://gitlab.com/msclock/pytools/commit/345dd560a2871f2934fad4fd3e3728e0daae83bc))
* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v24 ([7c03d0c](https://gitlab.com/msclock/pytools/commit/7c03d0c2afaba2a4bd6f8af7a96452d42f88596e))


### CI

* change npm source ([b71bd3d](https://gitlab.com/msclock/pytools/commit/b71bd3d9f53c4009afb7ed45648ad9ceec4c9ba6))

## [1.0.31](https://gitlab.com/msclock/pytools/compare/v1.0.30...v1.0.31) (2024-01-03)


### Dependencies

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.65 ([734c622](https://gitlab.com/msclock/pytools/commit/734c6226f35a223e673be1d8c29b516514b900f1))
* **deps:** update dependency pymilvus to v2.3.5 ([6970741](https://gitlab.com/msclock/pytools/commit/69707419b73bbd0fa75399b649ab34740723cf38))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.11 ([ce8274e](https://gitlab.com/msclock/pytools/commit/ce8274eefea9ecc38129620ef50b98428bb24e7c))
* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v23.12.1 ([f7c2644](https://gitlab.com/msclock/pytools/commit/f7c2644c7de3e10e83b503f696b7f85d5c01aa9f))

## [1.0.30](https://gitlab.com/msclock/pytools/compare/v1.0.29...v1.0.30) (2023-12-22)


### Dependencies

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.63 ([6391759](https://gitlab.com/msclock/pytools/commit/6391759a3b6fdc32452fd39288571780a01a1df3))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.9 ([717f0de](https://gitlab.com/msclock/pytools/commit/717f0ded8d4d8fd744231b70e05688f7dd6f2756))

## [1.0.29](https://gitlab.com/msclock/pytools/compare/v1.0.28...v1.0.29) (2023-12-14)


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.8 ([f3fc31b](https://gitlab.com/msclock/pytools/commit/f3fc31bc1f618a1f2099ff4b2f184393cd360a6e))

## [1.0.28](https://gitlab.com/msclock/pytools/compare/v1.0.27...v1.0.28) (2023-12-13)


### Dependencies

* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v23.12.0 ([83a3642](https://gitlab.com/msclock/pytools/commit/83a36423441a94f8c779dc30cceef9cefe86502c))
* **deps:** update quay.io/coreos/etcd docker tag to v3.5.11 ([5a43a5b](https://gitlab.com/msclock/pytools/commit/5a43a5b983c93e2ae45025a647d9a599ed285c53))

## [1.0.27](https://gitlab.com/msclock/pytools/compare/v1.0.26...v1.0.27) (2023-12-05)


### Dependencies

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.53 ([5b8d0de](https://gitlab.com/msclock/pytools/commit/5b8d0de396f87b88ef6ba65c7d27583a2c9cd15b))
* **deps:** update dependency pymilvus to v2.3.4 ([12dc891](https://gitlab.com/msclock/pytools/commit/12dc8916b0d4bd86271d73aa2ded3b0b81afd4c1))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.7 ([3cba8bf](https://gitlab.com/msclock/pytools/commit/3cba8bf5784ffe10b9e921792b96c4bc3aa5f7d6))

## [1.0.26](https://gitlab.com/msclock/pytools/compare/v1.0.25...v1.0.26) (2023-11-29)


### Dependencies

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.45 ([21309df](https://gitlab.com/msclock/pytools/commit/21309df89c19f18ba5c3f72151bb249ad87891dd))


### Chores

* configure renovate ([5e3858d](https://gitlab.com/msclock/pytools/commit/5e3858ddc764aeb7afa0c19af949806546d5dce2))

## [1.0.25](https://gitlab.com/msclock/pytools/compare/v1.0.24...v1.0.25) (2023-11-28)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.44 ([38614ba](https://gitlab.com/msclock/pytools/commit/38614ba963c0bbea5418fb2187aa2dc869f468b1))

## [1.0.24](https://gitlab.com/msclock/pytools/compare/v1.0.23...v1.0.24) (2023-11-27)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.42 ([5c6e88c](https://gitlab.com/msclock/pytools/commit/5c6e88c8067fc160a9356bfd9c00c7a7e3bf9518))


### CI

* change github mirror ([9fb3a74](https://gitlab.com/msclock/pytools/commit/9fb3a748b637229dbe595474de4160482449952d))

## [1.0.23](https://gitlab.com/msclock/pytools/compare/v1.0.22...v1.0.23) (2023-11-24)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.35 ([1e359e4](https://gitlab.com/msclock/pytools/commit/1e359e48b03403b78fb4a3911d37476bb3fb3bcc))

## [1.0.22](https://gitlab.com/msclock/pytools/compare/v1.0.21...v1.0.22) (2023-11-20)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.32 ([d3e59d8](https://gitlab.com/msclock/pytools/commit/d3e59d8c803c69cf009671a032e02c797d305175))


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.6 ([eb612fc](https://gitlab.com/msclock/pytools/commit/eb612fc21deebcc05ae01bf3ce72b7fb04e529cc))

## [1.0.21](https://gitlab.com/msclock/pytools/compare/v1.0.20...v1.0.21) (2023-11-12)


### Dependencies

* **deps:** update dependency pymilvus to v2.3.3 ([23549d5](https://gitlab.com/msclock/pytools/commit/23549d544e13bf01d5163601d93c20338d6df079))
* **deps:** update milvusdb/milvus docker tag to v2.3.3 ([f3bfe33](https://gitlab.com/msclock/pytools/commit/f3bfe33b75bb9820e26d946163618e54c4735774))

## [1.0.20](https://gitlab.com/msclock/pytools/compare/v1.0.19...v1.0.20) (2023-11-10)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.31 ([1f67ef5](https://gitlab.com/msclock/pytools/commit/1f67ef531fdc3c492ec00f050e35af4ed0bad13c))

## [1.0.19](https://gitlab.com/msclock/pytools/compare/v1.0.18...v1.0.19) (2023-11-10)


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.5 ([34b2f07](https://gitlab.com/msclock/pytools/commit/34b2f0783927be9e2def1ccef64f0176c59b8711))

## [1.0.18](https://gitlab.com/msclock/pytools/compare/v1.0.17...v1.0.18) (2023-11-08)


### Dependencies

* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v23.11.0 ([23e31d2](https://gitlab.com/msclock/pytools/commit/23e31d23c9bbffc0adb5ff86e1bca76830881d62))

## [1.0.17](https://gitlab.com/msclock/pytools/compare/v1.0.16...v1.0.17) (2023-11-08)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.30 ([1e7540b](https://gitlab.com/msclock/pytools/commit/1e7540b9e3df441825f4ea35618fc6c7a27c7f42))


### CI

* **fix:** reviews for pages ([5e29839](https://gitlab.com/msclock/pytools/commit/5e298395b5971540d59e2346459d31fdbaf95309))

## [1.0.16](https://gitlab.com/msclock/pytools/compare/v1.0.15...v1.0.16) (2023-11-05)


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.4 ([833c893](https://gitlab.com/msclock/pytools/commit/833c893e2ad1e002b8d235a632359ff5fd6a54c1))

## [1.0.15](https://gitlab.com/msclock/pytools/compare/v1.0.14...v1.0.15) (2023-11-02)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.29 ([badda17](https://gitlab.com/msclock/pytools/commit/badda1724a8e56a2fb94d36396491abb6814ddc9))

## [1.0.14](https://gitlab.com/msclock/pytools/compare/v1.0.13...v1.0.14) (2023-11-02)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.27 ([bfe3641](https://gitlab.com/msclock/pytools/commit/bfe364179082ef4230cc52bc843afb4269d567dd))


### Dependencies

* **deps:** update dependency pymilvus to v2.3.2 ([790c814](https://gitlab.com/msclock/pytools/commit/790c8148eec738831da20bbc38761eb7b20e27b0))
* **deps:** update milvusdb/milvus docker tag to v2.3.2 ([1396530](https://gitlab.com/msclock/pytools/commit/1396530810bccca75c2c84e732aa1dbd0b805a63))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.3 ([ac2d463](https://gitlab.com/msclock/pytools/commit/ac2d4639835392cbded4b16eeb846006ab30cacd))
* **deps:** update quay.io/coreos/etcd docker tag to v3.5.10 ([5f771e0](https://gitlab.com/msclock/pytools/commit/5f771e0140c56c3ca408ff57e0bdbb98e1c0f09b))

## [1.0.13](https://gitlab.com/msclock/pytools/compare/v1.0.12...v1.0.13) (2023-10-25)


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.2 ([1ccc9de](https://gitlab.com/msclock/pytools/commit/1ccc9de3e3f05f10a1f7195249d6261a1b0166cb))

## [1.0.12](https://gitlab.com/msclock/pytools/compare/v1.0.11...v1.0.12) (2023-10-24)


### Dependencies

* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v23.10.1 ([a540829](https://gitlab.com/msclock/pytools/commit/a540829c0f586afa773b4e0e5ad9e54b4af2539f))

## [1.0.11](https://gitlab.com/msclock/pytools/compare/v1.0.10...v1.0.11) (2023-10-22)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.23 ([1787ee2](https://gitlab.com/msclock/pytools/commit/1787ee204261938352439ce61dc8bba85bc6ad45))

## [1.0.10](https://gitlab.com/msclock/pytools/compare/v1.0.9...v1.0.10) (2023-10-21)


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.1 ([4d4d7ee](https://gitlab.com/msclock/pytools/commit/4d4d7ee1b4ff7a392a9073bcc2c77c2cbc6d2647))

## [1.0.9](https://gitlab.com/msclock/pytools/compare/v1.0.8...v1.0.9) (2023-10-19)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.20 ([92700c0](https://gitlab.com/msclock/pytools/commit/92700c08ecb9f5b7ccf57eeee7f5ce8af1d1998e))


### Dependencies

* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v23.10.0 ([dae064f](https://gitlab.com/msclock/pytools/commit/dae064fc00fc17553f6f637b207ca4c90c0c5e33))

## [1.0.8](https://gitlab.com/msclock/pytools/compare/v1.0.7...v1.0.8) (2023-10-17)


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.1.0 ([fccb584](https://gitlab.com/msclock/pytools/commit/fccb58498478e996f9c81e7ac4eb2fe8657b8b9f))

## [1.0.7](https://gitlab.com/msclock/pytools/compare/v1.0.6...v1.0.7) (2023-10-15)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.18 ([81e6744](https://gitlab.com/msclock/pytools/commit/81e6744430447e2c440bffd05939281278b5520b))


### Docs

* fix nuitka debug mode section newlines ([1955956](https://gitlab.com/msclock/pytools/commit/1955956ba49059ebb3ccbbe2ee74c989ef22804d))

## [1.0.6](https://gitlab.com/msclock/pytools/compare/v1.0.5...v1.0.6) (2023-10-09)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.15 ([d68d72b](https://gitlab.com/msclock/pytools/commit/d68d72b4c5f00089a513eeb7c6d6db1a79d13505))


### Dependencies

* **deps:** update pre-commit hook pre-commit/pre-commit-hooks to v4.5.0 ([c0f87f5](https://gitlab.com/msclock/pytools/commit/c0f87f5c3045d4a55312de68e5a929d50605b5b9))

## [1.0.5](https://gitlab.com/msclock/pytools/compare/v1.0.4...v1.0.5) (2023-10-07)


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.13 ([43c6d47](https://gitlab.com/msclock/pytools/commit/43c6d474be7d3ccb29b4ed79ab44f6c1cf33802a))


### Dependencies

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.0.292 ([95b1401](https://gitlab.com/msclock/pytools/commit/95b1401800722578210f4244b4f87330297f7241))
* **deps:** update pre-commit hook pre-commit/pre-commit-hooks to v4.4.0 ([c7c42d7](https://gitlab.com/msclock/pytools/commit/c7c42d745078a9cf3e467748960aa7b1aa52f3fc))
* **deps:** update pre-commit hook psf/black-pre-commit-mirror to v23.9.1 ([03db0fb](https://gitlab.com/msclock/pytools/commit/03db0fbf84bb7de92508b6f5d1b89aa8f3e5b72d))


### Chores

* enable pre-commit update based on renovate ([538a4e5](https://gitlab.com/msclock/pytools/commit/538a4e5c26805152c169e3a9e8dd2cb547fd4d44))

## [1.0.4](https://gitlab.com/msclock/pytools/compare/v1.0.3...v1.0.4) (2023-10-05)


### Style

* indent 2 with json and js files ([e672754](https://gitlab.com/msclock/pytools/commit/e6727543d061581fca73353de27bda5f04d93ed0))


### Performance

* **deps:** update dependency msclock/gitlab-ci-templates to v2.5.11 ([00a2991](https://gitlab.com/msclock/pytools/commit/00a2991c44c2f29dad66e3105be64cf64f4f7cb9))
