# Nuitka

For secure reasons, sources sometimes need to be converted to executables or shared modules that prevents them from being tampered with or leaked.

There are several tools for this. The most popular are pyinstaller and nuitka. Here [nuitka](https://nuitka.net/index.html) is used.

!!! note "prequisites for nuitka"
    ```bash
    pip install nuitka
    ```

## Convert to executable

Using nuitka convert the sources to executables. Commonly input main source for nuitka is just enough to compile.

```bash
python -m nuitka main.py
```

### option `--standalone`

With the option `--standalone`, final executable can be distributed without any dependencies.

```bash
python -m nuitka  --standalone main.py
```
After converting, the executable is named `main.bin` located in `./main.dist` that can be executed as the commond `./main.dist/main.bin`.

### option `--follow-imports`

`--follow-imports` enable follow imports automatically so that all dependencies are compiled. And the executable can also be distributed with all dependencies automatically if no necessary dependencies are excluded with `--nofollow-import-to`.

```bash
python -m nuitka --follow-imports main.py
```

After converting with the above options, the executable is named `main.bin` located in the current directory.

More use cases refer to <https://nuitka.net/doc/user-manual.html#use-cases>.

## Include data files

Some packages need data files to be copied to the bin directory. Usage is like the below.

#### using cmdline options

Using the below commonds, the specified data files are copied to the bin directory automatically.
```bash
# Copy md files in the sources to . data folder of bin directory
nuitka3 --standalone  --include-data-files=*.md=. main.py
# Copy md files in the sources to meta folder of bin directory
nuitka3 --standalone --include-data-files=*.md=meta/ main.py
# Copy package data into subpackage directory of bin directory
nuitka3 --standalone --include-package-data=subpackage main.py
```

When using pyproject.toml or setup.py, the data files are copied to the bin directory refer to [setuptools wheels](https://nuitka.net/doc/user-manual.html#use-case-5-setuptools-wheels)

links:

- <https://nuitka.net/doc/user-manual.html#missing-data-files-in-standalone>

#### using config file

If some dependencies (dll/data) are missing after conversion, you can add them to the configuration file.

```bash
# using custom configuration user.nuitka-package.config.yml
python -m nuitka \
    --standalone \
    --user-package-configuration-file=plugins/user.nuitka-package.config.yml \
    main.py
```

??? info "Custom configuration user.nuitka-package.config.yml"

    Copy md files in the sources to . data folder of bin directory automatically.
    ```python
    --8<-- "nuitka/plugins/user.nuitka-package.config.yml"
    ```

links:

- <https://github.com/Nuitka/Nuitka/issues/2403>


## Package practice

Normally, public packages are not necessary to compiled into the final binary because of **the long compile time**, so it is recommended to exclude them with the option `--nofollow-import-to`(no option `--standalone` in this case). But those common packages are essential to the generated bin in the runtime, so make sure to include them by using the options with a custom configuration file.

## Debug mode

Using the below options, the debug mode can be enabled.

- `--show-source-changes`: Show source changes during compilation.
- `--show-modules`: Show imported modules.

## Plugin system

Plugin system gives a way to extend functionality of nuitka that makes users work easier.

links:

- [create a user plugin](https://github.com/Nuitka/Nuitka/blob/develop/UserPlugin-Creation.rst)

### User plugin

There's a user plugin from ``plugins/user_plugin.py`` which is used to print a line when a math function is called.

??? info "A user plugin to trace info"
    ```python
    --8<-- "nuitka/plugins/user_plugin.py"
    ```

### Embed authorization validation

Thanks to plugin system, we can add authorization validation easily and gracefully.

#### Use custom configuration file

First of all, a authorization validation script is required. Here we use pyauthorize to create a simple validation script that will be embeded to the configuration file. See [demo](https://msclock.gitlab.io/pytools/nuitka/hello/#use-config-file).

??? info "Authorization configuration authorizer.nuitka-package.config.yml"
    ```python
    --8<-- "nuitka/plugins/authorizer.nuitka-package.config.yml"
    ```

What the script does is to check if the user is authorized to run the program. When users run the program, the script will check if the user is authorized to run the program with the license file `license.json`. If not, it will print a authorizing token and exit with error code 1 that means the token need to provide to vendor to generate the license file. See [demo](https://msclock.gitlab.io/pytools/nuitka/hello/#write-a-plugin).

#### Use a plugin of nuitka

Basically, writing a authorization plugin is the same as using the configuration file.

??? info "Authorization plugin authorizer_plugin.py"
    ```python
    --8<-- "nuitka/plugins/authorizer_plugin.py"
    ```

## Where else to look

There are more [examples](https://gitlab.com/msclock/pytools/-/tree/master/nuitka/).
